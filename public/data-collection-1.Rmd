---
title: "Collection and Analysis of<br>Digital Behavioral Data"
subtitle: "Data Collection I (Introduction to R)"
author: "Long Nguyen, Zaza Zindel"
date: "20 April 2023"
output:
  xaringan::moon_reader:
    css:
      - assets/xaringan-unibi.css
      - assets/width.css
      - https://rsms.me/inter/inter.css
      - https://cdn.jsdelivr.net/npm/@xz/fonts@1/serve/ibm-plex-mono.min.css
    nature:
      highlightStyle: github
      highlightLines: true
      ratio: 16:9
      countIncrementalSlides: false
      titleSlideClass: ["left", "bottom"]
---

```{r setup, include=FALSE}
options(
  htmltools.dir.version = FALSE,
  max.print = 64
)

knitr::opts_chunk$set(
  fig.width = 5.67, fig.asp = .618, fig.retina = 3,
  fig.align = "center",
  out.width = "100%",
  dev = "ragg_png",
  dev.args = list(bg = "transparent"),
  comment = "",
  cache = FALSE,
  echo = TRUE,
  message = FALSE, 
  warning = FALSE,
  hiline = TRUE
)

library(tidyverse)
theme_set(lo.ng::theme_long())
```

```{r xaringan-extra, echo=FALSE}
xaringanExtra::use_logo(
  width = "200px",
  image_url = "assets/faksoz.png",
  position = xaringanExtra::css_position(right = ".5em"),
  exclude_class = NULL
)
xaringanExtra::use_progress_bar(color = "#bfd02f", location = "bottom", height = "5px")
xaringanExtra::use_panelset()
xaringanExtra::use_extra_styles(
  hover_code_line = TRUE
)
```

```{css echo=FALSE}
pre {
  overflow: auto;
}
```

## Outline

```{r echo=FALSE}
lo.ng::render_toc(
  here::here("public/data-collection-1.Rmd"),
  toc_depth = 1
)
```

---

`r flipbookr::chunk_reveal("wa-example", widths = c(5, 4), title = "### Motivating example:<br>Number of messages per day in a WhatsApp chat")`

```{r wa-example, include=FALSE}
library(tidyverse)

read_csv(here::here("public/data/wa-chat.csv")) |>
  filter(year == 2018) |>
  mutate(date = as.Date(
    paste(year, month, day, sep = "-")
  )) |>
  summarise(n = n(), .by = c(person, date)) |>
  ggplot(aes(x = date, y = n, fill = person)) +
  geom_col(position = "dodge")
```

---
class: inverse center middle

# Getting familiar with R and RStudio

---

### R, RStudio

- **R**: the programming language

  <img src="https://cran.rstudio.com/Rlogo.svg" height=40>

--

- **RStudio**: the integrated development environment (IDE)

  <img src="https://www.rstudio.com/wp-content/uploads/2018/10/RStudio-Logo-flat.svg" height=40>

--

👉 Use **RStudio** to write, save, and run **R** code

---

### Start R with a blank state

.pull-left[
**Tools** → **Global Options**:

  <img src="https://rstats.wtf/img/rstudio-workspace.png" height=400>  
  .smaller[(Source: https://rstats.wtf/source-and-blank-slates.html)]
]

--

.pull-right[
#### Why?

- A polluted workspace can make your workflow unreproducible

- The code and data needed to reproduce your workflow are saved explicitly anyway
]

---

### Use RStudio Projects

.pull-left[
1) **File** → **New Project...**

  <img src="http://www.rstudio.com/images/docs/projects_new.png" height=280>  
  .smaller[(Source: https://support.posit.co/hc/en-us/articles/200526207-Using-Projects)]

2) **New Directory** → **New Project**
]
--

.pull-right[
#### Why?

- Data management: everything needed (code, data, and files) is in one single, organised directory

- Workspace management: project-specific **working directory**; no need for `setwd("path/that/only/works/on/my/machine")`

  <img src="https://evalsp23.classes.andrewheiss.com/files/img/example/working-directory.png" height=60>

- Reproducibility: share your code and data with others more easily
]

---

### Further readings

- [_What They Forgot to Teach You About R_](https://rstats.wtf/) by Jenny Bryan et al., Chapter 1–4

---
class: inverse center middle

# R programming basics

---
class: inverse center middle

## Functions

---

### Call a function

- Calculate the square root of 69420

--

```{r}
sqrt(69420)
```

--

- Write "data" in UPPERCASE

--

```{r}
toupper("data")
```

---

### Help pages

```r
?sqrt
```

--

```
Miscellaneous Mathematical Functions

Description:

     'abs(x)' computes the absolute value of x, 'sqrt(x)' computes the
     (principal) square root of x, sqrt{x}.

     The naming follows the standard for computer languages such as C
     or Fortran.

Usage:

     abs(x)
     sqrt(x)
     
Arguments:

       x: a numeric or 'complex' vector or array.
```

---

### Function arguments

```r
?sum
```

```
Sum of Vector Elements

Description:

     'sum' returns the sum of all the values present in its arguments.

Usage:

     sum(..., na.rm = FALSE)
     
Arguments:

     ...: numeric or complex or logical vectors.

   na.rm: logical.  Should missing values (including 'NaN') be removed?
```

--

- `na.rm` is an optional argument: comes with a default value (`FALSE`)

---

### Function arguments

`rnorm()` is a function that generates random numbers from a normal distribution. Fix the error in the code below:

```r
rnorm(10, mu = 100, sd = 50)
```

---
class: inverse center middle

## Objects

---

### Creating an object

- In R you save results by creating an object

- Use the assignment operator (`<-`) to store a value in an object:

```{r}
magic_number <- sqrt(69420)
```

--

### Using an object

```{r}
magic_number
```

```{r}
another_number <- magic_number
another_number ^ 100
```

---
class: inverse center middle

## Vectors

---

### Vectors

- R stores every value as a vector

- A single value, e.g. `1`, is actually a vector of length one

---

### Creating a vector

- `c()` function combines values into a single vector:

```{r}
c(1, 2, 3)
```

--

- Use the `:` shortcut to create a vector of continuous integers

```{r}
1:10
```

---

### Subsetting vector

```{r}
my_vec <- c(1, 2, 4, 8, 16)
```

--

- Extract the fourth element of `my_vec`:

```{r}
my_vec[4]
```

--

- Use a vector to extract the multiple elements of `my_vec`:

```{r}
my_vec[c(1, 2, 5)]
```

---
class: inverse center middle

## Data types

---

### Basic types

- **Atomic vectors**: every value is of the same type

  - `logical`: `c(TRUE, FALSE, NA)`

--

  - `numeric`<sup class="smaller">[*]</sup>: `c(3.14, 42, NA_real_)`

--

  - `character`: `c("Germany", "France", NA_character_)`

--

- `function`

--

- `list` (generic vector): can store elements of mixed types

```{r}
list("a", 1:3)
```

---

### Compound types (examples)

- `data.frame`

```{r}
mtcars
```

--

- `Date`

```{r}
as.Date("2023-04-20") + 1
```

---
class: inverse center middle

## Packages

---

### Loading packages

- At startup, R loads only a core set of functions and objects, known as **Base R**

- Other functions and objects are stored in collections known as **packages**

- To use these external functions, you must first load the package they come in with the `library()` function:

```r
library(tidyverse)
```

---

### Loading packages

What does this common error message suggest?

`object _____ does not exist.`

---

### Installing packages

```r
install.packages("tidyverse")
```

---
class: inverse center middle

# Working with data using **tidyverse**

---
class: inverse center middle

## Importing data

---

### CSV files

- CSV: comma-separated values

- Header row: column names

- Following rows: data

--

#### Example CSV file

👉 https://long_nguyen.pages.ub.uni-bielefeld.de/dbd-ss23/data/wa-chat.csv

```{r echo=FALSE}
readLines(here::here("public/data/wa-chat.csv")) |>
  head(5) |>
  c("...") |>
  cat(sep = "\n")
```

.smaller[(Source for jokes: https://icanhazdadjoke.com/)]

---

### Reading data from a CSV file

```{r eval=FALSE}
chat <- read_csv("https://long_nguyen.pages.ub.uni-bielefeld.de/dbd-ss23/data/wa-chat.csv")

chat
```

```{r echo=FALSE}
chat <- read_csv(here::here("public/data/wa-chat.csv"))
print(chat, n = 6)
```

---
class: inverse middle center

## Extracting variables and cases

---

### `select()`: Extracting columns

- First argument (`.data`): name of a data frame

- `...` argument(s): name(s) of the columns to extract – _not_ in quotation marks

--

```{r eval=FALSE}
select(chat, person, hour)
```

```{r echo=FALSE}
select(chat, person, hour) |>
  print(n = 6)
```

---

### `select()`: Extracting columns

- Place a minus sign before a column name to select every column but that column

```{r}
select(chat, -c(person, message))
```

---

#### Other `select()` helpers

.pull-left[
```{r eval=FALSE}
select(chat, year:minute)
```

```{r echo=FALSE}
select(chat, year:minute) |> print(n = 2)
```

```{r eval=FALSE}
select(chat, starts_with("m"))
```

```{r echo=FALSE}
select(chat, starts_with("m")) |> print(n = 2)
```
]

.pull-right[
```{r eval=FALSE}
select(chat, ends_with("e"))
```

```{r echo=FALSE}
select(chat, ends_with("e")) |> print(n = 2)
```

```{r eval=FALSE}
select(chat, contains("ji"))
```

```{r echo=FALSE}
select(chat, contains("ji")) |> print(n = 2)
```
]

---

### `filter()`: Extracting rows

- First argument (`.data`): name of a data frame

- `...` argument(s): logical test(s)

- Keeps every row for which the tests return `TRUE`

--

```{r eval=FALSE}
filter(chat, person == "Long")
```

```{r echo=FALSE}
filter(chat, person == "Long") |>
  print(n = 6)
```

---

#### Logical tests

|            |                                 |
|:-----------|:--------------------------------|
|`x == y`    |Is x equal to y?                 |
|`x != y`    |Is x not equal to y?             |
|`x > y`     |Is x greater than y?             |
|`x >= y`    |Is x greater than or equal to y? |
|`x < y`     |Is x less than y?                |
|`x <= y`    |Is x less than or equal to y?    |
|`is.na(x)`  |Is x an NA?                      |

---

#### Combining logical tests with boolean operators

|                    |                                         |
|:-------------------|:----------------------------------------|
|`!A`                |Is A not true?                           |
|`A & B`             |Are both A **and** B true?               |
|<span class="remark-inline-code">A &#124; B<span> |Is A **or** B (or both) true? |
|`xor(A, B)`         |Is **one and only one** of A and B true? |
|`x %in% c(a, b, c)` |Is x **in** the set of a, b, and c?      |
|`any(A, B, C)`      |Are **any** of A, B, or C true?          |
|`all(A, B, C)`      |Are **all** of A, B, and C true?         |

---

#### Exercise

Filter for messages sent on **9 and 10 September 2019**.

---

### `arrange()`: Reordering rows

- First argument (`.data`): name of a data frame

- `...` argument(s): column name(s) – _not_ in quotation marks

- Reorders rows by the values of the column names provided

--

```{r eval=FALSE}
arrange(chat, hour, minute)
```

```{r echo=FALSE}
arrange(chat, hour, minute) |>
  print(n = 6)
```

---

#### `desc()`: arranging in opposite order

```{r}
arrange(chat, desc(n_emojis))
```

---

### The pipe `|>`

- `tidyverse` functions are easy to use in a step-by-step fashion:

```{r}
chat_late <- filter(chat, hour > 22 | hour < 6)
chat_late <- select(chat_late, person, year:minute)
chat_late <- arrange(chat_late, hour)
```

```{r eval=FALSE}
chat_late
```

```{r echo=FALSE}
print(chat_late, n = 3)
```

--

→ inefficient way to write code

---

### The pipe `|>`

- One way to avoid redundancy:

```r
arrange(select(filter(chat, hour > 22 | hour < 6), person, year:minute), hour)
```

--

😵

---

### The pipe `|>`

```{r eval=FALSE}
chat |>
  filter(hour > 22 | hour < 6) |>
  select(person, year:minute) |>
  arrange(hour)
```

```{r echo=FALSE}
print(chat_late, n = 6)
```

---

### The pipe `|>`

`x |> f(y)` is equivalent to `f(x, y)`

--

<br>

This:

```r
thing |>
  firstly_do(something) |>
  secondly_do(something) |>
  thirdly_do(something)
```

is equivalent to this:

```r
thirdly_do(secondly_do(firstly_do(thing, something), something), something)
```

---
class: inverse center middle

## Deriving information

---

### `summarise()`:<br>Calculating single value out of multiple rows

Each named argument is set to an R expression that generates **a single value** → turned into a column in the new data frame

--

```{r}
chat |>
  filter(year == 2021) |>
  summarise(total = sum(n_emojis), max = max(n_emojis), mean = mean(n_emojis))
```

---

#### Summary functions


- Measures of location: `mean(x)`, `median(x)`, `quantile(x, 0.25)`, `min(x)`, `max(x)`

--

- Measures of spread: `var(x)`, `sd(x)`, `mad(x)`, `IQR(x)`

--

- Measures of position: `first(x)`, `nth(x, 2)`, `last(x)`

--

- Counts: `n()`, `n_distinct(x)`

--

- Counts and proportions of logical values:

  - `sum(x == y)`: number of `TRUE`s returned by a logical test
  
  - `mean(x == y)`: proportion of `TRUE`s returned by a logical test

---

#### Exercise

- How many years are there in the chat?

--

- What proportion of messages has at least one emoji?

--

- When was the last message sent by Person 2?

---

### `summarise(.by = <group of rows>)`

.pull-left[
```{r eval=FALSE}
chat |>
  mutate(timestamp = as_datetime(
    paste(year, month, day, hour, minute),
    format = "%Y %m %d %H %M"
  )) |>
  summarise(
    last = max(timestamp)
  )
```
]

.pull-right[
```{r echo=FALSE}
chat |>
  mutate(timestamp = as_datetime(
    paste(year, month, day, hour, minute),
    format = "%Y %m %d %H %M"
  )) |>
  summarise(
    last = max(timestamp)
  )
```
]

---

### `summarise(.by = <group of rows>)`

.pull-left[
```{r eval=FALSE}
chat |>
  mutate(timestamp = as_datetime(
    paste(year, month, day, hour, minute),
    format = "%Y %m %d %H %M"
  )) |>
  summarise(
    last = max(timestamp),
    .by = person #<<
  )
```
]

.pull-right[
```{r echo=FALSE}
chat |>
  mutate(timestamp = as_datetime(
    paste(year, month, day, hour, minute),
    format = "%Y %m %d %H %M"
  )) |>
  summarise(
    last = max(timestamp),
    .by = person #<<
  )
```
]

--

- `.by` takes column name(s) (like `select()` and `arrange()`)

- Each group is a set of rows that share identical combinations of values in the specified columns

---

### `summarise(.by = <group of rows>)`

.pull-left[
```{r eval=FALSE}
chat |>
  mutate(timestamp = as_datetime(
    paste(year, month, day, hour, minute),
    format = "%Y %m %d %H %M"
  )) |>
  summarise(
    last = max(timestamp),
    .by = c(person, year) #<<
  )
```
]

.pull-right[
```{r echo=FALSE}
chat |>
  mutate(timestamp = as_datetime(
    paste(year, month, day, hour, minute),
    format = "%Y %m %d %H %M"
  )) |>
  summarise(
    last = max(timestamp),
    .by = c(person, year) #<<
  )
```
]

---

#### Exercise

- How many messages did each person send?

- What proportion of messages did I send each year?

---

### `mutate()`:<br>Creating columns that are functions of existing ones

- Syntax similar to `summarise()`, but the expression in each named argument should return either **n values**, where n is the **number of rows**, or **a single value**

- Output data frame has the same number of rows as input data frame

--

```{r}
chat |>
  summarise(prop = mean(person == "Long"), .by = year) |>
  mutate(percent = round(prop * 100, 1)) #<<
```

---

#### Vectorised functions

Take vectors of input and **return vectors** of values

Examples:

- Arithmetic operators: `+`, `-`, `*`, `/`, `^`

- Modular arithmetic: `%/%` (integer division), `%%` (remainder)

- Logical comparisons: `<`, `<=`, `>`, `>=`, `!=`

- Logarithms: `log(x)`, `log2(x)`, `log10(x)`

- Offsets: `lead(x)`, `lag(x)`

- Cumulative aggregates: `cumsum(x)`, `cumprod(x)`, `cummin(x)`, `cummax(x)`, `cummean(x)`

- Ranking: `row_number(x)`, `min_rank(x)`, `dense_rank(x)`

---

#### Exercise

When did each person reach 100 emojis?

(Hint: `mutate()` also has a `.by` argument)

---

## Assignment

1. Write an R script that answers all the exercise questions in the slides (there should be 7 questions).

2. At the end of this script, think of a question that can be answered by combining at least 4 of the 5 functions `select()`, `arrange()`, `filter`, `mutate()`, and `summarise()` – then write the code to answer it.
