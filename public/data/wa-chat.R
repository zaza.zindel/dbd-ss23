library(tidyverse)

chat_raw <- tibble(raw = read_lines("public/assets/wa-chat.txt")) |>
  slice(-1)

set.seed(42)
jokes <- dadjokeapi::groan_search("") |>
  slice_sample(n = nrow(chat_raw), replace = TRUE) |>
  select(-id) |>
  mutate(len_rank = vctrs::vec_rank(nchar(joke), ties = "sequential"))

date_regex <- "^\\d{2}/\\d{2}/\\d{4}, \\d{2}:\\d{2}(?= - )"
person_regex <- "(?<=\\d - ).+?(?=:)"
emoji_regex <- "(?=[^0-9\\*\\#])\\p{Emoji}"

chat <- chat_raw |>
  mutate(
    person = str_extract(raw, person_regex),
    time = str_extract(raw, date_regex),
    message = sub(paste0(date_regex, " - ", person_regex, ": "), "", raw, perl = TRUE),
    len_rank = vctrs::vec_rank(nchar(message), ties = "sequential"),
    is_url = grepl("https://", message, fixed = TRUE),
    emojis = map_chr(str_extract_all(message, emoji_regex), paste0, collapse = "")
  ) |>
  left_join(jokes, by = join_by(len_rank)) |>
  mutate(
    message = case_when(
      message == "<Media omitted>" ~ message,
      is_url ~ paste0("https://www.", gsub("\\W", "-", tolower(joke), perl = TRUE)) |>
        gsub(pattern = "-+", replacement = "-", perl = TRUE) |>
        gsub(pattern = ".-", replacement = ".", fixed = TRUE) |>
        sub(pattern = "-$", replacement = "", perl = TRUE) |>
        paste0(".com"),
      nzchar(emojis) ~ paste(joke, emojis),
      TRUE ~ joke
    ),
    person = if_else(is.na(person) | person == "Long", person, "Person 2")
  )

chat |>
  mutate(out = if_else(
    is.na(time),
    message,
    paste0(time, " - ", person, ": ", message)
  )) |>
  pull(out) |>
  write_lines("public/data/wa-chat-1.txt")

chat |>
  mutate(
    time = dmy_hm(time),
    n_emojis = nchar(emojis)
  ) |>
  fill(person, time) |>
  mutate(across(time, lst(year, month, day, hour, minute), .names = "{.fn}")) |>
  select(person, year, month, day, hour, minute, message, n_emojis) |>
  write_csv("public/data/wa-chat.csv")
